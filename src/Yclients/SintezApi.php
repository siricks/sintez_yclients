<?php
namespace Yclients;
/**
 * Class SintezApi - упрощенный класс для работы с API
 * @package Yclients
 */
class SintezApi {
	private $companyId = 66318;//id компании
	private $tokenPartner = 'f68pa3zky825jebsekmz';//ключ доступа компании
	private $api;//переменная с базовым классом yclients

	public function __construct() {
		$this->api = new YclientsApi($this->tokenPartner);
	}

	/**
	 * Получить всех доступных сотрудников (ШЛЕМЫ) для бронирования
	 * @return array
	 * @throws YclientsException
	 */
	public function getAllBookStaff() {
		return $this->api->getBookStaff($this->getCompanyId());
	}

	/**
	 * Получить все доступные услуги для бронирования
	 *
	 * @param null $helmets_ids
	 *
	 * @return array
	 * @throws YclientsException
	 */
	public function getAllBookServices($helmets_ids = null){
		if ($helmets_ids) {
			$helmets_services = [];
			foreach ($helmets_ids as $helmets_id) {
				$helmets_services[$helmets_id] = $this->api->getBookServices($this->getCompanyId(), $helmets_id);
			}
			return $helmets_services;
		} else {
			return $this->api->getBookServices($this->getCompanyId());
		}
	}

	/**
	 * Получение времени записи к ОДНОМУ сотруднику (ШЛЕМУ)
	 * @param $staffId - ID сотрудника. Фильтр по идентификатору сотрудника
	 * @param \DateTime $date - Фильтр по месяцу бронирования (например '2015-09-01')
	 * @param null $serviceIds - ID услуг. Фильтр по списку идентификаторов уже
	 *                            выбранных (в рамках одной записи) услуг. Имеет
	 *                            смысл если зада фильтр по мастеру и дате.
	 * @param null $eventIds- ID акций. Фильтр по списку идентификаторов уже выбранных
	 *                          (в рамках одной записи) акций. Имеет смысл если зада
	 *                          фильтр по мастеру и дате.
	 *
	 * @return array
	 * @throws YclientsException
	 */
	public function getBookTimes($staffId, \DateTime $date, $serviceIds = null, $eventIds = null){
		return $this->api->getBookTimes($this->getCompanyId(), $staffId, $date, $serviceIds, $eventIds);
	}

	/**
	 * Получаем время бронирования набора шлемов
	 * @param $helmets
	 *
	 * @return array
	 * @throws YclientsException
	 */
	public function getBookTimesForHelmets($helmets) {
		$helmets_time = [];
		foreach ($helmets as $helmet) {
			$helmet['date'] = date_create_from_format('Y-m-d',$helmet['date']);
			$helmets_time[$helmet['staffId']] = $this->getBookTimes($helmet['staffId'], $helmet['date'], $helmet['serviceIds'], $helmet['eventIds']);
		}
		return $helmets_time;
	}

	/**
	 * Получаем даты бронирования для набора шлемов
	 * @param array $helmets
	 *
	 * @return array
	 * @throws YclientsException
	 */
	public function getBookDateForHelmets(array $helmets) {
		$helmets_date = [];
		foreach ($helmets as $helmet) {
			$helmets_date[$helmet['staffId']] = $this->api->getBookDates($this->getCompanyId(), $helmet['staffId'], $helmet['serviceIds'], $helmet['date'], $helmet['eventIds']);
		}
		return $helmets_date;
	}

	/**
	 * Создание записи без кода подтверждения и apiID
	 *
	 * @param $person
	 * @param $appointments
	 * @param $notify
	 * @param $comment
	 *
	 * @return array
	 * @throws YclientsException
	 */
	public function postBookRecord($person, $appointments, $notify, $comment){
		return $this->api->postBookRecord($this->getCompanyId(), $person, $appointments, null, $notify, $comment, null);
	}

	/**
	 * Преобразование массива с шлемами для создания записи
	 * @param array $helmets
	 *
	 * @return array
	 */
	public function convertHelmetsToAppointments(array $helmets){
		$appointments = [];
		$i = 1;
		foreach ($helmets as $helmet) {
			if (isset($helmet['staffId']) &&
			    $helmet['serviceIds'] &&
			    $helmet['datetime']
			) {
				$appointments[$i]['id'] = $i;
				$appointments[$i]['staff_id'] = $helmet['staffId'];
				$appointments[$i]['services'] = $helmet['serviceIds'];
				$appointments[$i]['datetime'] = $helmet['datetime'];
				$appointments[$i]['eventIds'] = $helmet['eventIds'];
				$i++;
			}
		}

		return $appointments;
	}

	/**
	 * Проверка записи
	 *
	 * @param array $appointments
	 *
	 * @return array
	 * @throws YclientsException
	 */
	public function postBookCheck(array $appointments){
		return $this->api->postBookCheck($this->getCompanyId(), $appointments);
	}


	/**
	 * @return int
	 */
	public function getCompanyId() {
		return $this->companyId;
	}

	/**
	 * @param int $companyId
	 */
	public function setCompanyId( $companyId ) {
		$this->companyId = $companyId;
	}

	/**
	 * @return string
	 */
	public function getTokenPartner() {
		return $this->tokenPartner;
	}

	/**
	 * @param string $tokenPartner
	 */
	public function setTokenPartner( $tokenPartner ) {
		$this->tokenPartner = $tokenPartner;
	}

}