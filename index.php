<?php
//Пример простой работы с API через упрощенный класс

include_once "src/Yclients/YclientsApi.php";
include_once "src/Yclients/SintezApi.php";

$sintez = new \Yclients\SintezApi();

//Получаю все шлемы
$helmets = $sintez->getAllBookStaff();
if (count($helmets)) {
	foreach ($helmets as $helmet) {
		echo $helmet['id'] . '  -  ';
		echo $helmet['name'];
		echo $helmet['specialization'];
		echo '<img src="'.$helmet['avatar'].'" style="border-radius: 50%;">';
		echo '<br>';
	}
}
//Передаю массив с id шлемов, вывожу все доступные услуги для них
$services = $sintez->getAllBookServices([115182, 115183, 115184, 241876, 241882]);
if (count($services)) {
	if (isset($services['services'])) {
		foreach ($services['services'] as $service) {
			echo $service['id'] . '  -  ';
			echo $service['title'];
			echo $service['price_min'];
			echo '<br>';
		}
	} else {
		foreach ($services as $helmet_service) {
			if ($helmet_service['services']) {
				foreach ($helmet_service['services'] as $service) {
					echo $service['id'] . '  -  ';
					echo $service['title'];
					echo $service['price_min'];
					echo '<br>';
				}
			}
			echo '<hr>';
		}
	}
}

//Создаю фикстуру массива со шлемами и выбранной (одной на всех) услугой
$helmets = [
	['staffId' => 115182, 'serviceIds' => [550570] ],
	['staffId' => 241876, 'serviceIds' => [550570] ],
];
foreach ($helmets as &$helmet) {
	if (!isset($helmet['serviceIds'])) {
		$helmet['serviceIds'] = null;
	}
	if (!isset($helmet['date'])) {
		$helmet['date'] = null;
	}

	if (!isset($helmet['eventIds'])) {
		$helmet['eventIds'] = null;
	}
}

//Вывожу массив с доступными датами бронирования
$dates = $sintez->getBookDateForHelmets($helmets);
var_dump($dates);

//Добавляю в фикстуру одну на всех дату бронирования
foreach ($helmets as &$helmet) {
	$helmet['date'] = '2017-12-30';
}

//Вывожу доступное на дату время бронирования
$times = $sintez->getBookTimesForHelmets($helmets);
var_dump($times);

//Добавляю в фикстуру одно на всех время бронирования
foreach ($helmets as &$helmet) {
	$helmet['datetime'] = '2017-12-30T12:00:00+04:00';
}

//Пример фикстуры записи
$appointments = [
	['id'=> 1, 'services' => [550569], 'events' => [], 'staff_id' => 115182, 'datetime' => '2017-12-30T12:00:00+04:00'],
	['id'=> 1, 'services' => [550569], 'events' => [], 'staff_id' => 241876, 'datetime' => '2017-12-30T12:00:00+04:00'],
];

//Создаю фикстуры для записи на шлем
$person = ['phone' => '79100502211', 'fullname' => 'Test', 'email' => 'test@test.test'];//Поля формы
$notify = ['notify_by_email' => 1];//Время уведомления на почту
$comment = 'Comment test by API';//Комментарий к заказу

//Преобразовываю текущий массив записей в формат для записи
//$appointments = $sintez->convertHelmetsToAppointments($helmets);
//var_dump($appointments);

//Проверка параметров
$check = $sintez->postBookCheck($appointments);
var_dump($check);

//Делаем запись
//$record = $sintez->postBookRecord($person, $appointments, $notify, $comment);
